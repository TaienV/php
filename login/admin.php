<?php
session_start();
    $errmsg = "";

    $key = sprintf('%04X%04X%04X%04X%04X%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));

    if (!isset($_SESSION["UID"])) header("Location:index.php");
    else if ($_SESSION['Role'] != 1) header("Location:member.php");

    if (isset($_POST["btnAdd"]))
    {
        if (!empty($_POST["txtFullName"])) $fullName = $_POST["txtFullName"];
        else $errmsg = "Name is required.";

        if (!empty($_POST["txtEmail"])) $email = $_POST["txtEmail"];
        else $errmsg = "Email is required.";

        if (!empty($_POST["txtPassword"])) $pwd = $_POST["txtPassword"];
        else $errmsg = "Password is required.";

        if ($pwd != $_POST["txtPassword2"]) $errmsg = "Passwords do not match.";

        if (!empty($_POST["txtRole"])) $role = $_POST["txtRole"];
        else $errmsg = "Role is required.";

        if ($errmsg=='')
        {
            include '../includes/dbConn.php';

            try{
                $dbo = new PDO($dsn, $username, $password, $options);

                $sql = $dbo->prepare("insert into tblMemberLogin (memberName,memberEmail,memberPassword,memberKey,roleID) value (:Name,:Email,:Password,:Key,:Role)");
                $sql->bindValue(":Name",$fullName);
                $sql->bindValue(":Email",$email);
                $sql->bindValue(":Password",md5($pwd . $key));
                $sql->bindValue(":Key",$key);
                $sql->bindValue(":Role",$role);
                $sql->execute();

                $fullName = '';
                $email = '';
                $password = '';
                $role = '';
                $errmsg = 'User successfully added.';
            } catch (PDOException $e)
            {
                $error = $e->getMessage();
                echo $error;
            }
        }
    }

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tyler's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <h1>Admin Page</h1>
    <h3 id="error"><?=$errmsg?></h3>
    <form method="post">
        <table border="1" width="100%">
            <tr height="100">
                <th colspan="2"><h3>Add User</h3></th>
            </tr>
            <tr height="40">
                <th>Full Name</th>
                <td><input id="txtFullName" name="txtFullName" type="text" size="50" required placeholder="full name"></td>
            </tr>
            <tr height="40">
                <th>Email Address</th>
                <td><input id="txtEmail" name="txtEmail" type="email" size="50" required placeholder="email@address.com"></td>
            </tr>
            <tr height="40">
                <th>Password</th>
                <td><input id="txtPassword" name="txtPassword" type="password" size="50" required placeholder="password"></td>
            <tr height="40">
                <th>Retype Password</th>
                <td><input id="txtPassword2" name="txtPassword2" type="password" size="50" required placeholder="re-type password"></td>
            </tr>
            <tr height="40">
                <th>Role</th>
                <td>
                    <select id="txtRole" name="txtRole">
                        <?php
                            try{
                                include '../includes/dbConn.php';

                                $dbo = new PDO($dsn, $username, $password, $options);

                                $sql = $dbo->prepare("select * from tblRole");
                                $sql->execute();

                                $row = $sql->fetch();

                                while ($row != null)
                                {
                                    echo '<option value="' . $row["roleID"] . '">' . $row["roleDescription"] . '</option>';
                                    $row = $sql->fetch();
                                }

                            } catch (PDOException $e)
                            {
                                $error = $e->getMessage();
                                echo $error;
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr height="60">
                <td colspan="2">
                    <input id="btnAdd" name="btnAdd" type="submit" value="Add User">
                </td>
            </tr>
        </table>
    </form>
    <br/>
</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>
