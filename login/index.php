<?php
session_start();

    if (isset($_POST["btnLogin"]))
    {
        if (isset($_POST["txtEmail"]) && !empty($_POST["txtEmail"]) && isset($_POST["txtPassword"]) && !empty($_POST["txtPassword"]))
        {
            $email = $_POST["txtEmail"];
            $pwd = $_POST["txtPassword"];
            $errmsg = '';

            include '../includes/dbConn.php';

            try{
                $dbo = new PDO($dsn, $username, $password, $options);

                $sql = $dbo->prepare("select memberID, memberPassword, memberKey, roleID from tblMemberLogin where memberEmail = :Email");
                $sql->bindValue(":Email",$email);
                $sql->execute();

                $row = $sql->fetch();

                if ($row != null) {
                    $hashedPassword = md5($pwd . $row['memberKey']);
                    if ($hashedPassword == $row['memberPassword'])
                    {
                        $_SESSION['UID'] = $row['memberID'];
                        $_SESSION['Role'] = $row['roleID'];
                        if ($_SESSION['Role'] == 1) //admin
                        {
                            header("Location:admin.php");
                        }
                        else
                        {
                            header("Location:member.php");
                        }
                    }
                    else $errmsg = "Username or password is incorrect.";
                }
                else $errmsg = "Username or password is incorrect.";

            } catch (PDOException $e) {
                $error = $e->getMessage();
                echo $error;
            }
        }
    }


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tyler's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <form method="post">
        <h3 id="error"><?=$errmsg?></h3>
        <table border="1" width="100%">
            <tr height="100">
                <th colspan="2"><h3>User Login</h3></th>
            </tr>
            <tr height="40">
                <th>Email Address</th>
                <td><input id="txtEmail" name="txtEmail" type="text" size="50" required placeholder="e-mail address"></td>
            </tr>
            <tr height="40">
                <th>Password</th>
                <td><input id="txtPassword" name="txtPassword" type="password" size="50" required placeholder="password"></td>
            </tr>
            <tr height="60">
                <td colspan="2">
                    <input id="btnLogin" name="btnLogin" type="submit" value="Login">
                </td>
            </tr>
        </table>
        <input id="txtId" name="txtId" type="hidden" value="<?=$id?>">
    </form>
</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>
