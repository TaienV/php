<?php
session_start();

if (!isset($_SESSION["UID"])) header("Location:index.php");

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tyler's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <h1>Member Page</h1>

    <?php
    if ($_SESSION['Role'] == 3) echo '<p>Welcome, member!  You look great today!</p>';
    else echo '<p>Welcome operator!  Perhaps you\'d like to do some smooth operatin\'?';

    ?>
</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>
