<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Race extends CI_Model {

    public function get_race($id)
    {
        $this->load->database();

        try{
            $data = array('raceID'=>$id);
            $query = $this->db->get_where('tblRace',$data);
            return $query->result_array();
        } catch (PDOException $e) {
            return false;
        }
    }

    public function get_races()
    {
        $this->load->database();

        try{
            $query = $this->db->get('tblRace');
            return $query->result_array();

        } catch (PDOException $e) {
            return false;
        }
    }

    public function add_race($name, $location, $description, $date)
    {
        $this->load->database();

        try{
            $data=array('raceName'=>$name, 'raceLocation'=>$location, 'raceDescription'=>$description, 'raceDateTime'=>$date);
            $this->db->insert('tblRace',$data);
            return true;

        } catch (PDOException $e) {
            return false;
        }
    }

    public function update_race($id, $name, $location, $description, $date)
    {
        $this->load->database();

        try{
            $data=array('raceID'=>$id, 'raceName'=>$name, 'raceLocation'=>$location, 'raceDescription'=>$description, 'raceDateTime'=>$date);
            $this->db->where('raceID',$id);
            $this->db->update('tblRace',$data);
            //$this->db->update_where('tblRace',$data);
            return true;

        } catch (PDOException $e) {
            return false;
        }
    }

    public function delete_race($id)
    {
        $this->load->database();

        try{
            $data=array('raceId'=>$id);
            $this->db->delete('tblRace',$data);
            return true;

        } catch (PDOException $e) {
            return false;
        }
    }


}
