<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Model {

    public function user_login($email,$pwd)
    {
        $this->load->database();
        $this->load->library('session');

        try{
            $dbo = new PDO($this->db->dsn, $this->db->username, $this->db->password, $this->db->options);

            $sql = $dbo->prepare("select memberID, memberPassword, memberKey from tblMemberLogin where memberEmail = :Email and roleID = 2");
            $sql->bindValue(":Email",$email);
            $sql->execute();

            $row = $sql->fetch();

            if ($row != null) {
                $hashedPassword = md5($pwd . $row['memberKey']);
                if ($hashedPassword == $row['memberPassword'])
                {
                    $this->session->set_userdata(array('UID'=>$row['memberID']));
                    return true;
                }
                else return false;
            }
            else return false;

        } catch (PDOException $e) {
            return false;
        }
    }

    public function add_user($fullName,$email,$password)
    {
        $key = sprintf('%04X%04X%04X%04X%04X%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
        $this->load->database();
        $this->load->library('session');

        try{
            $dbo = new PDO($this->db->dsn, $this->db->username, $this->db->password, $this->db->options);

            $sql = $dbo->prepare("insert into tblMemberLogin (memberName,memberEmail,memberPassword,memberKey,roleID) value (:Name,:Email,:Password,:Key,:Role)");
            $sql->bindValue(":Name",$fullName);
            $sql->bindValue(":Email",$email);
            $sql->bindValue(":Password",md5($password . $key));
            $sql->bindValue(":Key",$key);
            $sql->bindValue(":Role",2);
            $sql->execute();

            return true;
        } catch (PDOException $e)
        {
            return false;
        }
    }
}
