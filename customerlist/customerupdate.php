<?php

    include '../includes/dbConn.php';

    if (isset($_POST["btnUpdate"]))
    {
        if (!isset($_POST["txtFirstName"]) || $_POST["txtFirstName"] == "" ||
            !isset($_POST["txtLastName"]) || $_POST["txtLastName"] == "" ||
            !isset($_POST["txtPhone"]) || $_POST["txtPhone"] == "" ||
            !isset($_POST["txtEmail"]) || $_POST["txtEmail"] == "" ||
            !isset($_POST["txtAddress"]) || $_POST["txtAddress"] == "" ||
            !isset($_POST["txtCity"]) || $_POST["txtCity"] == "" ||
            !isset($_POST["txtZip"]) || $_POST["txtZip"] == "" ||
            !isset($_POST["txtState"]) || $_POST["txtState"] == "" ||
            !isset($_POST["txtPassword"]) || $_POST["txtPassword"] == "" ||
            !isset($_POST["txtPasswordVerify"]) || $_POST["txtPasswordVerify"] == "")
        {
            echo "Not all required fields were filled.  Go back and make sure to enter all required info.  How did you even get to this screen?  The form should prevent this.";
            exit();
        }
        else
        {
            if ($_POST["txtPassword"] != $_POST["txtPasswordVerify"])
            {
                echo "Your password field did not match the verification field.  Please try again.";
                exit();
            }

            $key = sprintf('%04X%04X%04X%04X%04X%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
            $firstName = $_POST["txtFirstName"];
            $lastName = $_POST["txtLastName"];
            $address = $_POST["txtAddress"];
            $city = $_POST["txtCity"];
            $state = $_POST["txtState"];
            $zip = $_POST["txtZip"];
            $phone = $_POST["txtPhone"];
            $email = $_POST["txtEmail"];
            $pwd = md5($_POST["txtPassword"] . $key);
            $id = $_POST["txtId"];

            try{
                $dbo = new PDO($dsn, $username, $password, $options);

                $sql = $dbo->prepare("update tblCustomerList set customerFirstName = :FirstName, customerLastName = :LastName, customerAddress = :Address, customerCity = :City, customerState = :State, customerZip = :Zip, customerPhone = :Phone, customerEmail = :Email, customerPassword = :Password, customerPasswordSalt = :Salt where customerID = :Id");
                $sql->bindValue(":FirstName",$firstName);
                $sql->bindValue(":LastName",$lastName);
                $sql->bindValue(":Address",$address);
                $sql->bindValue(":City",$city);
                $sql->bindValue(":State",$state);
                $sql->bindValue(":Zip",$zip);
                $sql->bindValue(":Phone",$phone);
                $sql->bindValue(":Email",$email);
                $sql->bindValue(":Password",$pwd);
                $sql->bindValue(":Salt",$key);
                $sql->bindValue(":Id",$id);
                $sql->execute();
            } catch (PDOException $e)
            {
                $error = $e->getMessage();
                echo $error;
            }

            header("Location:customerlist.php");
        }
    }

    if (isset($_GET["id"]))
    {
        $id = $_GET["id"];
        try{
            $dbo = new PDO($dsn, $username, $password, $options);

            $sql = $dbo->prepare("select * from tblCustomerList where customerID = :Id");
            $sql->bindValue(":Id",$id);
            $sql->execute();
            $row = $sql->fetch();

            $firstName = $row["customerFirstName"];
            $lastName = $row["customerLastName"];
            $address = $row["customerAddress"];
            $city = $row["customerCity"];
            $state = $row["customerState"];
            $zip = $row["customerZip"];
            $phone = $row["customerPhone"];
            $email = $row["customerEmail"];
            $pwd = $row["customerPassword"];
        } catch (PDOException $e)
        {
            $error = $e->getMessage();
            echo $error;
        }
    }
    else header("Location:customerlist.php");

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tyler's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
    <style>
        input{
            float: right;
        }
        select{
            float: right;
        }
        th{
            text-align: left;
        }
        td{
            text-align: left;
        }
        #buttons{
            float: left;
        }
        fieldset{
            background-color: #DDDDDD;
        }
    </style>
    <script type="text/javascript">
        function DeleteCustomer(firstname, lastname, id){
            if (confirm("Do you want to delete " + firstname + " " + lastname + " from the database?")){
                document.location.href = "customerdelete.php?id=" + id;
            }
        }
    </script>
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <h3>Update Account</h3>
    <form method="post">
        <fieldset>
            <legend align="left">Customer</legend>
            <table width="100%">
                <tr>
                    <th>First Name:</th>
                    <td>
                        <input id="txtFirstName" name="txtFirstName" type="text" size="50" autofocus required value="<?=$firstName?>">
                    </td>
                </tr>
                <tr>
                    <th>Last Name:</th>
                    <td>
                        <input id="txtLastName" name="txtLastName" type="text" size="50" required value="<?=$lastName?>">
                    </td>
                </tr>
                <tr>
                    <th>Phone Number:</th>
                    <td>
                        <input id="txtPhone" name="txtPhone" type="text" size="50" placeholder="(123)456-7890" required value="<?=$phone?>">
                    </td>
                </tr>
                <tr>
                    <th>Email:</th>
                    <td>
                        <input id="txtEmail" name="txtEmail" type="email" size="50" placeholder="your@email.com" required value="<?=$email?>">
                    </td>
                </tr>
            </table>
        </fieldset>

        <fieldset>
            <legend align="left">Address</legend>
            <table width="100%">
                <tr>
                    <th>Address:</th>
                    <td>
                        <input id="txtAddress" name="txtAddress" type="text" size="50" placeholder="1234 Street Ave" required value="<?=$address?>">
                    </td>
                </tr>
                <tr>
                    <th>City:</th>
                    <td>
                        <input id="txtCity" name="txtCity" type="text" size="50" required value="<?=$city?>">
                    </td>
                </tr>
                <tr>
                    <th>Zipcode:</th>
                    <td>
                        <input id="txtZip" name="txtZip" type="text" size="50" required value="<?=$zip?>">
                    </td>
                </tr>
                <tr>
                    <th>State:</th>
                    <td>
                        <select id="txtState" name="txtState" width="50" required>
                            <option value="AL" <?=$state == "AL" ? 'selected' : ''?>>Alabama</option>
                            <option value="AK" <?=$state == "AK" ? 'selected' : ''?>>Alaska</option>
                            <option value="AZ" <?=$state == "AZ" ? 'selected' : ''?>>Arizona</option>
                            <option value="AR" <?=$state == "AR" ? 'selected' : ''?>>Arkansas</option>
                            <option value="CA" <?=$state == "CA" ? 'selected' : ''?>>California</option>
                            <option value="CO" <?=$state == "CO" ? 'selected' : ''?>>Colorado</option>
                            <option value="CT" <?=$state == "CT" ? 'selected' : ''?>>Connecticut</option>
                            <option value="DE" <?=$state == "DE" ? 'selected' : ''?>>Delaware</option>
                            <option value="DC" <?=$state == "DC" ? 'selected' : ''?>>District Of Columbia</option>
                            <option value="FL" <?=$state == "FL" ? 'selected' : ''?>>Florida</option>
                            <option value="GA" <?=$state == "GA" ? 'selected' : ''?>>Georgia</option>
                            <option value="HI" <?=$state == "HI" ? 'selected' : ''?>>Hawaii</option>
                            <option value="ID" <?=$state == "ID" ? 'selected' : ''?>>Idaho</option>
                            <option value="IL" <?=$state == "IL" ? 'selected' : ''?>>Illinois</option>
                            <option value="IN" <?=$state == "IN" ? 'selected' : ''?>>Indiana</option>
                            <option value="IA" <?=$state == "IA" ? 'selected' : ''?>>Iowa</option>
                            <option value="KS" <?=$state == "KS" ? 'selected' : ''?>>Kansas</option>
                            <option value="KY" <?=$state == "KY" ? 'selected' : ''?>>Kentucky</option>
                            <option value="LA" <?=$state == "LA" ? 'selected' : ''?>>Louisiana</option>
                            <option value="ME" <?=$state == "ME" ? 'selected' : ''?>>Maine</option>
                            <option value="MD" <?=$state == "MD" ? 'selected' : ''?>>Maryland</option>
                            <option value="MA" <?=$state == "MA" ? 'selected' : ''?>>Massachusetts</option>
                            <option value="MI" <?=$state == "MI" ? 'selected' : ''?>>Michigan</option>
                            <option value="MN" <?=$state == "MN" ? 'selected' : ''?>>Minnesota</option>
                            <option value="MS" <?=$state == "MS" ? 'selected' : ''?>>Mississippi</option>
                            <option value="MO" <?=$state == "MO" ? 'selected' : ''?>>Missouri</option>
                            <option value="MT" <?=$state == "MT" ? 'selected' : ''?>>Montana</option>
                            <option value="NE" <?=$state == "NE" ? 'selected' : ''?>>Nebraska</option>
                            <option value="NV" <?=$state == "NV" ? 'selected' : ''?>>Nevada</option>
                            <option value="NH" <?=$state == "NH" ? 'selected' : ''?>>New Hampshire</option>
                            <option value="NJ" <?=$state == "NJ" ? 'selected' : ''?>>New Jersey</option>
                            <option value="NM" <?=$state == "NM" ? 'selected' : ''?>>New Mexico</option>
                            <option value="NY" <?=$state == "NY" ? 'selected' : ''?>>New York</option>
                            <option value="NC" <?=$state == "NC" ? 'selected' : ''?>>North Carolina</option>
                            <option value="ND" <?=$state == "ND" ? 'selected' : ''?>>North Dakota</option>
                            <option value="OH" <?=$state == "OH" ? 'selected' : ''?>>Ohio</option>
                            <option value="OK" <?=$state == "OK" ? 'selected' : ''?>>Oklahoma</option>
                            <option value="OR" <?=$state == "OR" ? 'selected' : ''?>>Oregon</option>
                            <option value="PA" <?=$state == "PA" ? 'selected' : ''?>>Pennsylvania</option>
                            <option value="RI" <?=$state == "RI" ? 'selected' : ''?>>Rhode Island</option>
                            <option value="SC" <?=$state == "SC" ? 'selected' : ''?>>South Carolina</option>
                            <option value="SD" <?=$state == "SD" ? 'selected' : ''?>>South Dakota</option>
                            <option value="TN" <?=$state == "TN" ? 'selected' : ''?>>Tennessee</option>
                            <option value="TX" <?=$state == "TX" ? 'selected' : ''?>>Texas</option>
                            <option value="UT" <?=$state == "UT" ? 'selected' : ''?>>Utah</option>
                            <option value="VT" <?=$state == "VT" ? 'selected' : ''?>>Vermont</option>
                            <option value="VA" <?=$state == "VA" ? 'selected' : ''?>>Virginia</option>
                            <option value="WA" <?=$state == "WA" ? 'selected' : ''?>>Washington</option>
                            <option value="WV" <?=$state == "WV" ? 'selected' : ''?>>West Virginia</option>
                            <option value="WI" <?=$state == "WI" ? 'selected' : ''?>>Wisconsin</option>
                            <option value="WY" <?=$state == "WY" ? 'selected' : ''?>>Wyoming</option>
                        </select>
                    </td>
                </tr>
            </table>
        </fieldset>

        <fieldset>
            <legend align="left">Security</legend>
            <table width="100%">
                <tr>
                    <th>Password:</th>
                    <td>
                        <input id="txtPassword" name="txtPassword" type="password" size="50" placeholder="enter passsword" required>
                    </td>
                </tr>
                <tr>
                    <th>Password Verification:</th>
                    <td>
                        <input id="txtPasswordVerify" name="txtPasswordVerify" type="password" size="50" placeholder="re-enter password" required>
                    </td>
                </tr>
            </table>
        </fieldset>

        <div id="buttons">
            <input id="btnDelete" name="btnDelete" type="button" value="Delete Account" onclick="DeleteCustomer('<?=$firstName?>','<?=$lastName?>','<?=$id?>')">
            <input id="btnReset" name="btnReset" type="reset" value="Reset">
            <input id="btnUpdate" name="btnUpdate" type="submit" value="Update Account">
        </div>
        <input id="txtId" name="txtId" type="hidden" value="<?=$id?>">
    </form>
</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>
