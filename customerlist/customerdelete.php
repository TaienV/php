<?php
    if (isset($_GET["id"]))
    {
        $id= $_GET["id"];
        try{
            include '../includes/dbConn.php';

            $dbo = new PDO($dsn, $username, $password, $options);

            $sql = $dbo->prepare("delete from tblCustomerList where customerID = :Id");
            $sql->bindValue(":Id",$id);
            $sql->execute();
        } catch (PDOException $e)
        {
            $error = $e->getMessage();
            echo $error;
        }
    }
    header("Location:customerlist.php");
?>