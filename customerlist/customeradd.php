<?php
if (isset($_POST["btnAdd"]))
{
    if (!isset($_POST["txtFirstName"]) || $_POST["txtFirstName"] == "" ||
        !isset($_POST["txtLastName"]) || $_POST["txtLastName"] == "" ||
        !isset($_POST["txtPhone"]) || $_POST["txtPhone"] == "" ||
        !isset($_POST["txtEmail"]) || $_POST["txtEmail"] == "" ||
        !isset($_POST["txtAddress"]) || $_POST["txtAddress"] == "" ||
        !isset($_POST["txtCity"]) || $_POST["txtCity"] == "" ||
        !isset($_POST["txtZip"]) || $_POST["txtZip"] == "" ||
        !isset($_POST["txtState"]) || $_POST["txtState"] == "" ||
        !isset($_POST["txtPassword"]) || $_POST["txtPassword"] == "" ||
        !isset($_POST["txtPasswordVerify"]) || $_POST["txtPasswordVerify"] == "")
    {
        echo "Not all required fields were filled.  Go back and make sure to enter all required info.  How did you even get to this screen?  The form should prevent this.";
        exit();
    }
    else
    {
        if ($_POST["txtPassword"] != $_POST["txtPasswordVerify"])
        {
            echo "Your password field did not match the verification field.  Please try again.";
            exit();
        }

        $key = sprintf('%04X%04X%04X%04X%04X%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
        $firstName = $_POST["txtFirstName"];
        $lastName = $_POST["txtLastName"];
        $address = $_POST["txtAddress"];
        $city = $_POST["txtCity"];
        $state = $_POST["txtState"];
        $zip = $_POST["txtZip"];
        $phone = $_POST["txtPhone"];
        $email = $_POST["txtEmail"];
        $pwd = md5($_POST["txtPassword"] . $key);

        include '../includes/dbConn.php';

        try{
            $dbo = new PDO($dsn, $username, $password, $options);

            $sql = $dbo->prepare("insert into tblCustomerList (customerFirstName, customerLastName, customerAddress, customerCity, customerState, customerZip, customerPhone, customerEmail, customerPassword, customerPasswordSalt) value (:FirstName,:LastName,:Address,:City,:State,:Zip,:Phone,:Email,:Password,:Salt)");
            $sql->bindValue(":FirstName",$firstName);
            $sql->bindValue(":LastName",$lastName);
            $sql->bindValue(":Address",$address);
            $sql->bindValue(":City",$city);
            $sql->bindValue(":State",$state);
            $sql->bindValue(":Zip",$zip);
            $sql->bindValue(":Phone",$phone);
            $sql->bindValue(":Email",$email);
            $sql->bindValue(":Password",$pwd);
            $sql->bindValue(":Salt",$key);
            $sql->execute();
        } catch (PDOException $e)
        {
            $error = $e->getMessage();
            echo $error;
        }

        header("Location:customerlist.php");
    }
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tyler's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
    <style>
        input{
            float: right;
        }
        select{
            float: right;
        }
        th{
            text-align: left;
        }
        td{
            text-align: left;
        }
        #buttons{
            float: left;
        }
        fieldset{
            background-color: #DDDDDD;
        }
    </style>
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <h3>Create New Account</h3>
    <form method="post">
        <fieldset>
            <legend align="left">Customer</legend>
            <table width="100%">
                <tr>
                    <th>First Name:</th>
                    <td>
                        <input id="txtFirstName" name="txtFirstName" type="text" size="50" autofocus required>
                    </td>
                </tr>
                <tr>
                    <th>Last Name:</th>
                    <td>
                        <input id="txtLastName" name="txtLastName" type="text" size="50" required>
                    </td>
                </tr>
                <tr>
                    <th>Phone Number:</th>
                    <td>
                        <input id="txtPhone" name="txtPhone" type="text" size="50" placeholder="(123)456-7890" required>
                    </td>
                </tr>
                <tr>
                    <th>Email:</th>
                    <td>
                        <input id="txtEmail" name="txtEmail" type="email" size="50" placeholder="your@email.com" required>
                    </td>
                </tr>
            </table>
        </fieldset>

        <fieldset>
            <legend align="left">Address</legend>
            <table width="100%">
                <tr>
                    <th>Address:</th>
                    <td>
                        <input id="txtAddress" name="txtAddress" type="text" size="50" placeholder="1234 Street Ave" required>
                    </td>
                </tr>
                <tr>
                    <th>City:</th>
                    <td>
                        <input id="txtCity" name="txtCity" type="text" size="50" required>
                    </td>
                </tr>
                <tr>
                    <th>Zipcode:</th>
                    <td>
                        <input id="txtZip" name="txtZip" type="text" size="50" required>
                    </td>
                </tr>
                <tr>
                    <th>State:</th>
                    <td>
                        <select id="txtState" name="txtState" width="50" required>
                            <option value="AL">Alabama</option>
                            <option value="AK">Alaska</option>
                            <option value="AZ">Arizona</option>
                            <option value="AR">Arkansas</option>
                            <option value="CA">California</option>
                            <option value="CO">Colorado</option>
                            <option value="CT">Connecticut</option>
                            <option value="DE">Delaware</option>
                            <option value="DC">District Of Columbia</option>
                            <option value="FL">Florida</option>
                            <option value="GA">Georgia</option>
                            <option value="HI">Hawaii</option>
                            <option value="ID">Idaho</option>
                            <option value="IL">Illinois</option>
                            <option value="IN">Indiana</option>
                            <option value="IA">Iowa</option>
                            <option value="KS">Kansas</option>
                            <option value="KY">Kentucky</option>
                            <option value="LA">Louisiana</option>
                            <option value="ME">Maine</option>
                            <option value="MD">Maryland</option>
                            <option value="MA">Massachusetts</option>
                            <option value="MI">Michigan</option>
                            <option value="MN">Minnesota</option>
                            <option value="MS">Mississippi</option>
                            <option value="MO">Missouri</option>
                            <option value="MT">Montana</option>
                            <option value="NE">Nebraska</option>
                            <option value="NV">Nevada</option>
                            <option value="NH">New Hampshire</option>
                            <option value="NJ">New Jersey</option>
                            <option value="NM">New Mexico</option>
                            <option value="NY">New York</option>
                            <option value="NC">North Carolina</option>
                            <option value="ND">North Dakota</option>
                            <option value="OH">Ohio</option>
                            <option value="OK">Oklahoma</option>
                            <option value="OR">Oregon</option>
                            <option value="PA">Pennsylvania</option>
                            <option value="RI">Rhode Island</option>
                            <option value="SC">South Carolina</option>
                            <option value="SD">South Dakota</option>
                            <option value="TN">Tennessee</option>
                            <option value="TX">Texas</option>
                            <option value="UT">Utah</option>
                            <option value="VT">Vermont</option>
                            <option value="VA">Virginia</option>
                            <option value="WA">Washington</option>
                            <option value="WV">West Virginia</option>
                            <option value="WI">Wisconsin</option>
                            <option value="WY">Wyoming</option>
                        </select>
                    </td>
                </tr>
            </table>
        </fieldset>

        <fieldset>
            <legend align="left">Security</legend>
            <table width="100%">
                <tr>
                    <th>Password:</th>
                    <td>
                        <input id="txtPassword" name="txtPassword" type="password" size="50" placeholder="enter passsword" required>
                    </td>
                </tr>
                <tr>
                    <th>Password Verification:</th>
                    <td>
                        <input id="txtPasswordVerify" name="txtPasswordVerify" type="password" size="50" placeholder="re-enter password" required>
                    </td>
                </tr>
            </table>
        </fieldset>

        <div id="buttons">
            <input id="btnReset" name="btnReset" type="reset" value="Reset">
            <input id="btnAdd" name="btnAdd" type="submit" value="Create Account">
        </div>

    </form>
</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>
