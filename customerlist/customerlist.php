<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tyler's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
    <style>
        th{
            font-size: 12px;
        }
        td{
            font-size: 12px;
        }
    </style>
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <h3>Customer Listing</h3>
    <table border="1" width="100%">
        <tr>
            <th>CustomerID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Zip</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Password</th>
        </tr>
        <?php

        include '../includes/dbConn.php';

        try{
            $dbo = new PDO($dsn, $username, $password, $options);

            $sql = $dbo->prepare("select * from tblCustomerList");
            $sql->execute();

            $row = $sql->fetch();

            while ($row != null)
            {
                echo '<tr>';
                echo '<td><a href="customerupdate.php?id=' .  $row["customerID"] . '">' . $row["customerID"] . '</a></td>';
                echo '<td>' . $row["customerFirstName"] . '</td>';
                echo '<td>' . $row["customerLastName"] . '</td>';
                echo '<td>' . $row["customerAddress"] . '</td>';
                echo '<td>' . $row["customerCity"] . '</td>';
                echo '<td>' . $row["customerState"] . '</td>';
                echo '<td>' . $row["customerZip"] . '</td>';
                echo '<td>' . $row["customerPhone"] . '</td>';
                echo '<td>' . $row["customerEmail"] . '</td>';
                echo '<td>' . $row["customerPassword"] . '</td></tr>';
                $row = $sql->fetch();
            }

        } catch (PDOException $e)
        {
            $error = $e->getMessage();
            echo $error;
        }
        ?>
    </table>
    <br/><br/>
    <a href="customeradd.php">Add New Customer</a>

</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>
