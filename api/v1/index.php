<?php
require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

/*
 * Put = Update
 * Post = Create
 * Get = Read
 * Delete = Delete*/

$app = new \Slim\Slim();

$app->get('/getHello','getHello');
$app->get('/showMember/:MemberName','showMember');
$app->post('/addMember/:MemberName','addMember');
$app->post('/addJson/','addJson');
$app->delete('/delUser/:userId','delUser');
$app->get('/get_races','getRaces');
$app->get('/get_runners/:race_id','getRunners');
$app->post('/add_runner','addRunner');
$app->delete('/delete_runner','deleteRunner');

$app->run();

function getRaces()
{
    include '../../includes/dbConn.php';

    try{
        $dbo = new PDO($dsn, $username, $password, $options);

        $sql = $dbo->prepare("select * from tblRace");
        $sql->execute();
        $results = $sql->fetchAll();

        echo '{"Races":' . json_encode($results) . '}';
    } catch (PDOException $e) {
        $error = $e->getMessage();
        echo json_encode($error);
    }
}

function getRunners($race_id)
{
    include '../../includes/dbConn.php';

    try{
        $dbo = new PDO($dsn, $username, $password, $options);

        $sql = $dbo->prepare("select distinct ml.memberName, ml.memberEmail from tblMemberLogin ml
                              join tblMemberRace mr on ml.memberID = mr.memberID
                              where mr.roleID = 3 and mr.raceID = :raceID");
        $sql->bindValue(":raceID",$race_id);
        $sql->execute();
        $results = $sql->fetchAll();

        echo '{"Runners":' . json_encode($results) . '}';
    } catch (PDOException $e) {
        $error = $e->getMessage();
        echo json_encode($error);
    }
}

function addRunner()
{
    $request = \Slim\Slim::getInstance()->request();
    $post_json = json_decode($request->getBody(),true);

    $memberID = $post_json['memberID'];
    $raceID = $post_json['raceID'];
    $memberKey = $post_json['memberKey'];

    include '../../includes/dbConn.php';

    try{
        $dbo = new PDO($dsn, $username, $password, $options);

        $sql = $dbo->prepare("select distinct mr.raceID from tblMemberRace mr
                              join tblMemberLogin ml on mr.memberID = ml.memberID
                              where mr.raceID = :raceID and ml.memberKey = :memberKey
                              and mr.roleID = 2");
        $sql->bindValue(":raceID",$raceID);
        $sql->bindValue(":memberKey",$memberKey);
        $sql->execute();
        $results = $sql->fetch();

        if ($results == null)
        {
            echo "Bad API Key";
        }
        else
        {
            $sql = $dbo->prepare("insert into tblMemberRace (memberID, raceID, roleID) value (:memberID, :raceID, 3)");
            $sql->bindValue(":raceID",$raceID);
            $sql->bindValue(":memberID",$memberID);
            $sql->execute();
        }

    } catch (PDOException $e) {
        $error = $e->getMessage();
        echo json_encode($error);
    }
}

function deleteRunner()
{
    $request = \Slim\Slim::getInstance()->request();
    $post_json = json_decode($request->getBody(),true);

    $memberID = $post_json['memberID'];
    $raceID = $post_json['raceID'];
    $memberKey = $post_json['memberKey'];

    include '../../includes/dbConn.php';

    try{
        $dbo = new PDO($dsn, $username, $password, $options);

        $sql = $dbo->prepare("select distinct mr.raceID from tblMemberRace mr
                              join tblMemberLogin ml on mr.memberID = ml.memberID
                              where mr.raceID = :raceID and ml.memberKey = :memberKey
                              and mr.roleID = 2");
        $sql->bindValue(":raceID",$raceID);
        $sql->bindValue(":memberKey",$memberKey);
        $sql->execute();
        $results = $sql->fetch();

        if ($results == null)
        {
            echo "Bad API Key";
        }
        else
        {
            $sql = $dbo->prepare("delete from tblMemberRace where memberID = :memberID and raceID = :raceID");
            $sql->bindValue(":raceID",$raceID);
            $sql->bindValue(":memberID",$memberID);
            $sql->execute();
        }

    } catch (PDOException $e) {
        $error = $e->getMessage();
        echo json_encode($error);
    }
}

/* demo stuff */
function getHello()
{
    echo 'Hello World';
}

function showMember($MemberName)
{
    echo 'Hello ' . $MemberName;
}

function addMember($MemberName)
{
    echo 'Hello ' . $MemberName;
}

function addJson()
{
    $request = \Slim\Slim::getInstance()->request();
    $post_json = json_decode($request->getBody(),true);

    echo $post_json["fname"];
}

function delUser($userId)
{
    echo 'User ' . $userId . ' deleted.';
}

?>

