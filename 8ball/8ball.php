<?php
session_start(); //must be fully non-indented; no spaces
    //$arrUser = array("First"=>"Bob","Last"=>"Day","Address"=>"N/A");
    //echo "$arrUser[First] has many women, and his last name is $arrUser[Last]<br>";
    //echo '$arrUser[First] has many women, and his last name is $arrUser[Last]<br>';

    $response = array();
    $response[0] = "ASK AGAIN LATER";
    $response[1] = "YES";
    $response[2] = "NO";
    $response[3] = "IT APPEARS TO BE SO";
    $response[4] = "REPLY IS HAZY";
    $response[5] = "YES, DEFINITELY";
    $response[6] = "OUTLOOK IS GOOD";
    $response[7] = "MR. SPOCK SAYS IT IS LOGICAL";
    $response[8] = "DON'T ASK ME THAT";
    $response[9] = "I DON'T EVEN KNOW";
    $response[10] = "I CAN'T EVEN";
    $response[11] = "YES, IT WILL BE GREAT AGAIN";
    $response[12] = "NO, IT WILL NOT BE GREAT";
    $response[13] = "I SURE AS HELL HOPE SO";
    $response[14] = "PLEASE GOD NO";
    $response[15] = "I HAVE THE BEST ANSWERS";
    $response[16] = "I WOULDN'T CARE TO LIVE IF I THOUGHT THAT";
    $response[17] = "GRUMPY CAT SAYS 'NO.'";
    $response[18] = "WHAT DO YOU THINK?";
    $response[19] = "YOU BETCHA, CHAMP";
    $response[20] = "I HIGHLY DOUBT IT";
    $response[21] = "THAT'S NOT WHAT YOUR MOM SAID";
    $response[22] = "I DON'T EVEN KNOW WHAT THOSE WORDS MEAN";
    $response[23] = "APPARENTLY";
    $response[24] = "INCONCEIVABLE!!";
    $response[25] = "IF SHE WEIGHS THE SAME AS A DUCK";
    $response[26] = "MAKE IT SO";
    $response[27] = "YES, RESISTANCE IS FUTILE";
    $response[28] = "THIS IS WHY WE CAN'T HAVE NICE THINGS";
    $response[29] = "I CAN'T BELIEVE IT";
    $response[30] = "I'VE NEVER HEARD SOMETHING SO RIDICULOUS";
    $response[31] = "CASSH ME OUSSIDE, HOW BOW DAH";
    $response[32] = "NOT IF I HAVE ANYTHING TO SAY ABOUT IT";
    $response[33] = "I'D LOVE TO SEE THAT, BUT NO";
    $response[34] = "OF COURSE NOT, YOU ARE ENGLISH TYPES";
    $response[35] = "I WASN'T EXPECTING THE SPANISH INQUISITION";
    $response[36] = "I USED TO THINK SO, BUT THEN I TOOK AN ARROW IN THE KNEE";

    if (isset($_GET[txtQuestion]))
    {
        $question = $_GET[txtQuestion];
    }
    else
    {
        $question = "";
    }

    if (isset($_SESSION["PrevQuestion"]))
    {
        $PrevQuestion = $_SESSION["PrevQuestion"];
    }
    else
    {
        $PrevQuestion = "";
    }


    if ($question == "")
    {
        $answer = "Ask the 8-Ball a question...";
    }
    elseif (substr($question,-1)!="?") //-1 gets last character
    {
        $question = "";
        $answer = "That is not a question!  Please ask one with a ? at the end.";
    }
    elseif ($PrevQuestion == $question)
    {
        $question = "";
        $answer = "Please ask a NEW question!";
    }
    else
    {
        $random = rand(0,36);
        $answer = $response[$random];
        $_SESSION["PrevQuestion"] = $question;
    }
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tyler's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <h2>Magic 8-Ball</h2>
    <p><?=$question?></p>
    <marquee><?=$answer?></marquee>
    <p>Ask a question....</p>
    <form method="get" action="#">
        <input type="text" name="txtQuestion" id="txtQuestion" value="<?=$question?>"/> <br>
        <input type="submit" value="Ask the 8-Ball"/>
    </form>
</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>