<?php
session_start();
    if (isset($_POST[btnRoll]))
    {
        $areDiceRolled = true;
        // roll all dice
        $playerDie1 = mt_rand(1,6);
        $playerDie2 = mt_rand(1,6);
        $compDie1 = mt_rand(1,6);
        $compDie2 = mt_rand(1,6);
        $compDie3 = mt_rand(1,6);

        $playerTotal = $playerDie1 + $playerDie2;
        $compTotal = $compDie1 + $compDie2 + $compDie3;
    }
    else $areDiceRolled = false;
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tyler's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
   <div id="dice"/>
        <?php
            if ($areDiceRolled)
            {
                echo "<p>Your score: $playerTotal<br/>";
                echo "<img src=\"../images/dice_$playerDie1.png\"/>";
                echo "<img src=\"../images/dice_$playerDie2.png\"/><br/></p>";

                echo "<p>Computer score: $compTotal<br/>";
                echo "<img src=\"../images/dice_$compDie1.png\"/>";
                echo "<img src=\"../images/dice_$compDie2.png\"/>";
                echo "<img src=\"../images/dice_$compDie3.png\"/><br/></p>";

                if ($playerTotal > $compTotal) echo "<h1>You win!</h1>";
                elseif ($compTotal > $playerTotal) echo "<h1>You lose!</h1>";
                else echo "<h1>It's a tie!</h1>";

            }
            else
            {
                echo "<p>Roll the dice by clicking the button and try to beat the computer!</p>";
            }
        ?>
    </div>
    <form action="../dice/dice.php" method="post">
        <input type="submit" id="btnRoll" name="btnRoll" value="Roll Dice" />
    </form>
</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>
