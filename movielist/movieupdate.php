<?php

    include '../includes/dbConn.php';

    if (isset($_POST["btnUpdate"]))
    {
        if (isset($_POST["txtTitle"]) && $_POST["txtTitle"] != "" && isset($_POST["txtRating"]) && $_POST["txtTitle"] != "")
        {
            $title = $_POST["txtTitle"];
            $rating = $_POST["txtRating"];
            $id = $_POST["txtId"];

            try{
                $dbo = new PDO($dsn, $username, $password, $options);

                $sql = $dbo->prepare("update movielist set movieTitle = :Title, movieRating = :Rating where movieID = :Id");
                $sql->bindValue(":Title",$title);
                $sql->bindValue(":Rating",$rating);
                $sql->bindValue(":Id",$id);
                $sql->execute();
            } catch (PDOException $e)
            {
                $error = $e->getMessage();
                echo $error;
            }

            header("Location:movielist.php");
        }
    }

    if (isset($_GET["id"]))
    {
        $id= $_GET["id"];
        try{
            $dbo = new PDO($dsn, $username, $password, $options);

            $sql = $dbo->prepare("select * from movielist where movieID = :id");
            $sql->bindValue(":id",$id);
            $sql->execute();
            $row = $sql->fetch();

            $rating = $row["movieRating"];
            $title = $row["movieTitle"];
        } catch (PDOException $e)
        {
            $error = $e->getMessage();
            echo $error;
        }
    }
    else header("Location:movielist.php");
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tyler's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
    <script type="text/javascript">
        function DeleteMovie(title, id){
            if (confirm("Do you want to delete " + title + "?")){
                document.location.href = "moviedelete.php?id=" + id;
            }
        }
    </script>
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <form method="post">
        <table border="1" width="100%">
            <tr height="100">
                <th colspan="2"><h3>Update Movie</h3></th>
            </tr>
            <tr height="40">
                <th>Movie Title</th>
                <td><input id="txtTitle" name="txtTitle" type="text" size="50" required value="<?=$title?>"></td>
            </tr>
            <tr height="40">
                <th>Movie Rating</th>
                <td><input id="txtRating" name="txtRating" type="text" size="50" required value="<?=$rating?>"></td>
            </tr>
            <tr height="60">
                <td colspan="2">
                    <input id="btnUpdate" name="btnUpdate" type="submit" value="Update Movie">
                    <input id="btnDelete" name="btnDelete" type="button" value="Delete Movie" onclick="DeleteMovie('<?=$title?>','<?=$id?>')">
                </td>
            </tr>
        </table>
        <input id="txtId" name="txtId" type="hidden" value="<?=$id?>">
    </form>
</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>
