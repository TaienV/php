<?php
    if (isset($_POST["btnAdd"]))
    {
        if (isset($_POST["txtTitle"]) && $_POST["txtTitle"] != "" && isset($_POST["txtRating"]) && $_POST["txtTitle"] != "")
        {
            $title = $_POST["txtTitle"];
            $rating = $_POST["txtRating"];

            include '../includes/dbConn.php';

            try{
                $dbo = new PDO($dsn, $username, $password, $options);

                $sql = $dbo->prepare("insert into movielist (movieTitle, movieRating) value (:Title,:Rating)");
                $sql->bindValue(":Title",$title);
                $sql->bindValue(":Rating",$rating);
                $sql->execute();
            } catch (PDOException $e)
            {
                $error = $e->getMessage();
                echo $error;
            }

            header("Location:movielist.php");
        }
    }
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tyler's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <form method="post">
        <table border="1" width="100%">
            <tr height="100">
                <th colspan="2"><h3>Add New Movie</h3></th>
            </tr>
            <tr height="40">
                <th>Movie Title</th>
                <td><input id="txtTitle" name="txtTitle" type="text" size="50" autofocus required></td>
            </tr>
            <tr height="40">
                <th>Movie Rating</th>
                <td><input id="txtRating" name="txtRating" type="text" size="50" required></td>
            </tr>
            <tr height="60">
                <td colspan="2">
                    <input id="btnAdd" name="btnAdd" type="submit" value="Add New Movie">
                </td>
            </tr>
        </table>
    </form>
</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>
