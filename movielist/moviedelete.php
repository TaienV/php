<?php

    if (isset($_GET["id"]))
    {
        $id= $_GET["id"];
        try{
            include '../includes/dbConn.php';

            $dbo = new PDO($dsn, $username, $password, $options);

            $sql = $dbo->prepare("delete from movielist where movieID = :id");
            $sql->bindValue(":id",$id);
            $sql->execute();
        } catch (PDOException $e)
        {
            $error = $e->getMessage();
            echo $error;
        }
    }
    header("Location:movielist.php");

?>