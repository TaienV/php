<?php

    /*
     * Countdown timer ending on 5-17-2017 at 11:30am
     * (Time of our final class)
     */

    $secPerMin = 60;
    $secPerHour = 60 * $secPerMin;
    $secPerDay = 24 * $secPerHour;
    $secPerYear = 365 * $secPerDay;

    //current time
    $now = time();

    //burning man time
    $endOfSemester = mktime(11,30,0,5,17,2017);

    //number of seconds between now and then
    $seconds = $endOfSemester - $now;

    $years = floor($seconds / $secPerYear);
    $seconds -= $years * $secPerYear;

    $days = floor($seconds / $secPerDay);
    $seconds -= $days * $secPerDay;

    $hours = floor($seconds / $secPerHour);
    $seconds -= $hours * $secPerHour;

    $minutes = floor($seconds / $secPerMin);
    $seconds -= $minutes * $secPerMin;

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tyler's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <p>
    <h3>Time Until Final Class</h3>
    <?php
        echo "$years Years, $days Days, $hours Hours, $minutes Minutes, $seconds Seconds"
    ?></p>
</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>
