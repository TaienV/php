<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Tyler's Homepage</title>
        <link rel="stylesheet" type="text/css" href="./css/base.css">
    </head>
    <body>
        <header><?php include './includes/header.php' ?></header>
        <nav><?php include './includes/nav.php' ?></nav>
        <main>
            <p>
                <div id="homeImg"><img src="images/tyler.jpg" alt="Tyler Terrell"></div>
                Welcome to my homepage.  Here you will find my projects for the PHP course
                at FVTC.
            </p>
            <p>
                I am currently enrolled in the Software Development program at FVTC and
                will be graduating this semester.  I have been attending since 2008 when
                I returned to school, and have been mostly paying out of pocket for 6
                credits a semester for all these years.  While it took a bit longer, I saved
                a great deal of money and managed to keep myself on track despite losing
                my aid.
            </p>
            <p>
                My ultimate goal is to create my own PC or mobile games as a career,
                either working for a company or running a small indie team of my own.
                I have been slowly gaining programming experience in this area for the
                past 20 years.  I have done 3D map development (both brush-based and using
                more modern technologies such as Unity), coded in many languages (C++, C#, Java,
                ASP, SQL, and now PHP), run servers of multiple games, modded multiple games,
                and studied the subject extensively.  While I will have my FVTC degree to fall
                back on, I hope to release my game on Android before I graduate, at least in an Alpha
                or Beta format.
            </p>
            <p>
                I am looking forward to the knowledge I'll gain during this semester, provided
                our VMs don't lock up from being non-Genuine Windows and I can keep my
                repository on Bitbucket from getting screwed up by PHPStorm's weird Git
                interface. :)
            </p>
        </main>
        <footer><?php include './includes/footer.php' ?></footer>
    </body>
</html>
